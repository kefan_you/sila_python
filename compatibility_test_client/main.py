import contextlib
import time

from sila2_python_compatibility_test_server import Client
from sila2_python_compatibility_test_server.generated.authenticationtest import AuthenticationTestFeature
from sila2_python_compatibility_test_server.generated.errorhandlingtest import TestError

from sila2.framework import UndefinedExecutionError

client = Client("127.0.0.1", 50052, insecure=True)

# SiLA Service
client.SiLAService.GetFeatureDefinition("org.silastandard/core/SiLAService/v1")
client.SiLAService.GetFeatureDefinition("org.silastandard/test/MetadataProvider/v1")

original_server_name = client.SiLAService.ServerName.get()
client.SiLAService.SetServerName("SiLA is Awesome")
client.SiLAService.SetServerName(original_server_name)

client.SiLAService.ServerName.get()
client.SiLAService.ServerType.get()
client.SiLAService.ServerUUID.get()
client.SiLAService.ServerDescription.get()
client.SiLAService.ServerVersion.get()
client.SiLAService.ServerVendorURL.get()
client.SiLAService.ImplementedFeatures.get()

# Unobservable Command Test
client.UnobservableCommandTest.CommandWithoutParametersAndResponses()
client.UnobservableCommandTest.ConvertIntegerToString(12345)
client.UnobservableCommandTest.JoinIntegerAndString(123, "abc")
client.UnobservableCommandTest.SplitStringAfterFirstCharacter("")
client.UnobservableCommandTest.SplitStringAfterFirstCharacter("a")
client.UnobservableCommandTest.SplitStringAfterFirstCharacter("ab")
client.UnobservableCommandTest.SplitStringAfterFirstCharacter("abcde")

# Unobservable Property Test
client.UnobservablePropertyTest.AnswerToEverything.get()
client.UnobservablePropertyTest.SecondsSince1970.get()

# Metadata Provider Test
client.MetadataProvider.StringMetadata.get_affected_calls()
client.MetadataProvider.TwoIntegersMetadata.get_affected_calls()

# Metadata Consumer Test
client.MetadataConsumerTest.EchoStringMetadata(metadata=[client.MetadataProvider.StringMetadata("abc")])
client.MetadataConsumerTest.UnpackMetadata(
    metadata=[
        client.MetadataProvider.StringMetadata("abc"),
        client.MetadataProvider.TwoIntegersMetadata((123, 456)),
    ]
)
client.MetadataConsumerTest.ReceivedStringMetadata.get(metadata=[client.MetadataProvider.StringMetadata("abc")])
stream = client.MetadataConsumerTest.ReceivedStringMetadataAsCharacters.subscribe(
    metadata=[client.MetadataProvider.StringMetadata("abc")]
)
next(stream)
next(stream)
next(stream)

# Error Handling Test
with contextlib.suppress(TestError):
    client.ErrorHandlingTest.RaiseDefinedExecutionError()

instance = client.ErrorHandlingTest.RaiseDefinedExecutionErrorObservably()
with contextlib.suppress(TestError):
    instance.get_responses()

with contextlib.suppress(UndefinedExecutionError):
    client.ErrorHandlingTest.RaiseUndefinedExecutionError()

instance = client.ErrorHandlingTest.RaiseUndefinedExecutionErrorObservably()
with contextlib.suppress(UndefinedExecutionError):
    instance.get_responses()

with contextlib.suppress(TestError):
    client.ErrorHandlingTest.RaiseDefinedExecutionErrorOnGet.get()

stream = client.ErrorHandlingTest.RaiseDefinedExecutionErrorOnSubscribe.subscribe()
with contextlib.suppress(TestError):
    next(stream)

with contextlib.suppress(UndefinedExecutionError):
    client.ErrorHandlingTest.RaiseUndefinedExecutionErrorOnGet.get()

stream = client.ErrorHandlingTest.RaiseUndefinedExecutionErrorOnSubscribe.subscribe()
with contextlib.suppress(UndefinedExecutionError):
    next(stream)

stream = client.ErrorHandlingTest.RaiseDefinedExecutionErrorAfterValueWasSent.subscribe()
next(stream)
with contextlib.suppress(TestError):
    next(stream)

stream = client.ErrorHandlingTest.RaiseUndefinedExecutionErrorAfterValueWasSent.subscribe()
next(stream)
with contextlib.suppress(UndefinedExecutionError):
    next(stream)

# Observable Property Test
client.ObservablePropertyTest.FixedValue.get()

values = []
stream = client.ObservablePropertyTest.Alternating.subscribe(callbacks=[values.append])
next(stream)
next(stream)
next(stream)
stream.cancel()

values = []
stream = client.ObservablePropertyTest.Editable.subscribe(callbacks=[values.append])
client.ObservablePropertyTest.SetValue(1)
client.ObservablePropertyTest.SetValue(2)
client.ObservablePropertyTest.SetValue(3)
stream.cancel()

# Observable Command Test
count_instance = client.ObservableCommandTest.Count(5, 1)
for value in count_instance.subscribe_to_intermediate_responses():
    print(value, count_instance.estimated_remaining_time)
print(count_instance.get_responses())

echo_instance = client.ObservableCommandTest.EchoValueAfterDelay(3, 5)
while not echo_instance.done:
    time.sleep(0.5)
    print(echo_instance.estimated_remaining_time)
print(echo_instance.get_responses())

# Binary Transfer Test
client.BinaryTransferTest.EchoBinaryValue(b"abc")
client.BinaryTransferTest.EchoBinaryValue(b"abc" * 1_000_000)
client.BinaryTransferTest.BinaryValueDirectly.get()
client.BinaryTransferTest.BinaryValueDownload.get()
client.BinaryTransferTest.EchoBinaryAndMetadataString(b"abc", metadata=[client.BinaryTransferTest.String("abc")])
client.BinaryTransferTest.EchoBinaryAndMetadataString(
    b"abc" * 1_000_000, metadata=[client.BinaryTransferTest.String("abc")]
)
instance = client.BinaryTransferTest.EchoBinariesObservably([b"abc", b"abc" * 1_000_000, b"SiLA2_Test_String_Value"])
for item in instance.subscribe_to_intermediate_responses():
    print(len(item.Binary), instance.status, instance.progress, instance.estimated_remaining_time)
while not instance.done:
    print(instance.status, instance.progress, instance.estimated_remaining_time)
    time.sleep(0.1)
print(len(instance.get_responses().JointBinary))

# Authentication/Authorization
token = client.AuthenticationService.Login(
    UserIdentification="test",
    Password="test",  # noqa: S106, hardcoded password
    RequestedServer=client.SiLAService.ServerUUID.get(),
    RequestedFeatures=[AuthenticationTestFeature.fully_qualified_identifier],
).AccessToken
tokenMetadata = client.AuthorizationService.AccessToken(token)

client.AuthenticationTest.RequiresToken(metadata=[tokenMetadata])
client.AuthenticationTest.RequiresTokenForBinaryUpload(b"abc", metadata=[tokenMetadata])
client.AuthenticationTest.RequiresTokenForBinaryUpload(b"abc" * 1_000_000, metadata=[tokenMetadata])
client.AuthenticationService.Logout(token)
