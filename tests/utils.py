import random
import re
import socket
from concurrent.futures import ThreadPoolExecutor
from os.path import dirname, join
from types import ModuleType
from typing import Any, Tuple

import grpc

resources_dir = join(dirname(__file__), "resources")


def get_feature_definition_str(feature_identifier: str) -> str:
    with open(get_fdl_path(feature_identifier), encoding="utf-8") as fp:
        return fp.read()


def get_fdl_path(feature_identifier: str) -> str:
    return join(resources_dir, "feature_definitions", feature_identifier + ".sila.xml")


def generate_port() -> int:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        for port in random.sample(range(49152, 55000), 100):
            try:
                s.bind(("127.0.0.1", port))
                return port
            except OSError:  # port not free
                pass
    raise RuntimeError("Failed to find a free local port")


def create_server_stub(grpc_module: ModuleType, servicer) -> Tuple[grpc.Server, Any]:
    server, channel = create_grpc_server_channel()

    stub_cls = __get_first_attribute_that_matches(grpc_module, re.compile(".*Stub"))
    stub = stub_cls(channel)

    add_func = __get_first_attribute_that_matches(grpc_module, re.compile("add_.*_to_server"))
    add_func(servicer, server)

    server.start()

    return server, stub


def __get_first_attribute_that_matches(obj: Any, name_pattern: re.Pattern) -> Any:
    for name in dir(obj):
        if re.fullmatch(name_pattern, name):
            return getattr(obj, name)
    raise AttributeError(f"Object {obj!r} has no attribute matching {name_pattern}")


def create_grpc_server_channel() -> Tuple[grpc.Server, grpc.Channel]:
    port = generate_port()

    server = grpc.server(ThreadPoolExecutor(max_workers=100, thread_name_prefix="test-grpc-server-executor"))
    server.add_insecure_port(f"localhost:{port}")

    channel = grpc.insecure_channel(f"localhost:{port}")

    return server, channel
