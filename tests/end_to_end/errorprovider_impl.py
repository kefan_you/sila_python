from typing import Any, Dict

from sila2.framework.errors.command_execution_not_accepted import CommandExecutionNotAccepted
from sila2.framework.errors.defined_execution_error import DefinedExecutionError
from sila2.framework.errors.validation_error import ValidationError
from sila2.framework.feature import Feature
from sila2.framework.fully_qualified_identifier import FullyQualifiedIdentifier
from sila2.server.feature_implementation_base import FeatureImplementationBase
from tests.utils import get_feature_definition_str

ErrorProvider = Feature(get_feature_definition_str("ErrorProvider"))


class Uneven(DefinedExecutionError):
    def __init__(self, number: int):
        super().__init__(ErrorProvider.defined_execution_errors["Uneven"], f"{number} is uneven")


class TestError(DefinedExecutionError):
    def __init__(self):
        super().__init__(ErrorProvider.defined_execution_errors["TestError"], "An error for testing")


class ErrorProviderImpl(FeatureImplementationBase):
    def CheckIfEven(self, Number: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> bool:
        if Number == -2:
            raise CommandExecutionNotAccepted("Cannot execute CheckIfEven")
        if Number == -1:
            validation_error = ValidationError(
                "Number -1 is not allowed",
            )
            validation_error.parameter_fully_qualified_identifier = (
                ErrorProvider._unobservable_commands["CheckIfEven"].parameters.fields[0].fully_qualified_identifier
            )
            raise validation_error
        if Number == 0:
            raise ValueError("Number 0 is not allowed")  # client should raise UndefinedExecutionError
        if Number % 2 == 0:
            return True
        raise Uneven(Number)

    def get_UndefinedInvalidProperty(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> int:
        raise RuntimeError("Cannot access property")

    def get_DefinedInvalidProperty(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> int:
        raise TestError
