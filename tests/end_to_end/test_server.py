import pytest

from sila2.server import SilaServer
from tests.end_to_end.timer_impl import TimerFeature, TimerImpl
from tests.utils import generate_port


@pytest.fixture()
def server() -> SilaServer:
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

    return TestServer()


def test_add_implementation_twice(server):
    server.set_feature_implementation(TimerFeature, TimerImpl(server))

    with pytest.raises(RuntimeError):
        server.set_feature_implementation(TimerFeature, TimerImpl(server))


def test_add_implementation_while_running(server):
    server.start_insecure("127.0.0.1", generate_port(), enable_discovery=False)

    with pytest.raises(RuntimeError):
        server.set_feature_implementation(TimerFeature, TimerImpl(server))

    server.stop()


def test_stop_before_start(server):
    with pytest.raises(RuntimeError):
        server.stop()


def test_start_twice(server):
    server.start_insecure("127.0.0.1", generate_port(), enable_discovery=False)
    with pytest.raises(RuntimeError):
        server.start_insecure("127.0.0.1", generate_port(), enable_discovery=False)
    server.stop()
