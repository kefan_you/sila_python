import time
import uuid

import pytest
from sila2_example_server import Client as ExampleClient
from sila2_example_server import Server as ExampleServer

from sila2.client import SilaClient
from sila2.discovery.browser import SilaDiscoveryBrowser
from sila2.server.sila_server import SilaServer
from tests.utils import generate_port


@pytest.fixture()
def test_server() -> SilaServer:
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

    return TestServer()


def test_discovery_browser(test_server):
    server_uuid = test_server.server_uuid

    browser = SilaDiscoveryBrowser(insecure=True)

    with pytest.raises(TimeoutError):
        browser.find_server(timeout=0.1)

    try:
        port = generate_port()
        test_server.start_insecure("127.0.0.1", port)  # discovery should be enabled by default

        with pytest.raises(ValueError, match="Timeout must be non-negative"):
            browser.find_server(timeout=-1)

        with pytest.raises(TimeoutError, match="No suitable SiLA server was found"):
            browser.find_server(server_name="UnknownServer", timeout=0.1)
        with pytest.raises(TimeoutError, match="No suitable SiLA server was found"):
            browser.find_server(server_uuid=uuid.uuid4(), timeout=0.1)

        for kwargs in [
            {},
            {"server_name": "TestServer"},
            {"server_uuid": server_uuid},
            {"server_uuid": server_uuid, "server_name": "TestServer"},
        ]:
            client = browser.find_server(**kwargs, timeout=10)

            assert client.SiLAService.ServerName.get() == test_server.server_name
            assert client.SiLAService.ServerUUID.get() == str(server_uuid)

    finally:
        test_server.stop()

    time.sleep(1)  # browser must first handle the unregistration request sent on server stop
    with pytest.raises(TimeoutError):
        browser.find_server(server_name="TestServer", timeout=0.5)


def test_discovery_via_client():
    port = generate_port()
    server = ExampleServer()
    try:
        server.start("127.0.0.1", port)
        with pytest.warns(RuntimeWarning, match="untrusted certificate"):
            client = ExampleClient.discover(timeout=10)
        assert isinstance(client, ExampleClient)
    finally:
        server.stop()


def test_discovery_via_wrong_client(test_server):
    port = generate_port()
    try:
        test_server.start("127.0.0.1", port)

        # finds a server with missing required features
        # warns because it failed to connect
        # error because discovery timed out
        with pytest.raises(TimeoutError), pytest.warns(RuntimeWarning, match="does not implement .* required features"):
            ExampleClient.discover(timeout=10)

        with pytest.warns(RuntimeWarning, match="untrusted certificate"):  # untrusted certificate
            c = SilaClient.discover(timeout=10)  # works because SilaClient does not expect any features
        assert isinstance(c, SilaClient)
    finally:
        test_server.stop()
