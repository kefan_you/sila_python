from sila2.framework.data_types.binary import Binary
from sila2.framework.data_types.boolean import Boolean
from sila2.framework.data_types.date import Date
from sila2.framework.data_types.integer import Integer
from sila2.framework.data_types.real import Real
from sila2.framework.data_types.string import String
from sila2.framework.data_types.time import Time
from sila2.framework.data_types.timestamp import Timestamp


def test_from_fdl_node(basic_feature):
    assert isinstance(basic_feature._data_type_definitions["Bool"].data_type, Boolean)
    assert isinstance(basic_feature._data_type_definitions["Int"].data_type, Integer)
    assert isinstance(basic_feature._data_type_definitions["Float"].data_type, Real)
    assert isinstance(basic_feature._data_type_definitions["Str"].data_type, String)
    assert isinstance(basic_feature._data_type_definitions["Bytes"].data_type, Binary)
    assert isinstance(basic_feature._data_type_definitions["Date"].data_type, Date)
    assert isinstance(basic_feature._data_type_definitions["Time"].data_type, Time)
    assert isinstance(basic_feature._data_type_definitions["Datetime"].data_type, Timestamp)
