import pytest

from sila2.framework.command.observable_command import ObservableCommand
from sila2.framework.data_types.structure import Structure
from sila2.framework.feature import Feature
from sila2.framework.property.unobservable_property import UnobservableProperty
from tests.utils import get_feature_definition_str


@pytest.fixture()
def structure_feature() -> Feature:
    return Feature(get_feature_definition_str("Structure"))


def test(structure_feature):
    dtype = structure_feature._data_type_definitions["StructureType"]
    msg = dtype.to_message(1, [True, b"abc"], "abc")
    res = dtype.to_native_type(msg)

    assert res.Element1 == 1
    assert res.Element2.Subelement1 is True
    assert res.Element2.Subelement2 == b"abc"
    assert res.Element3 == "abc"

    with pytest.raises(TypeError):
        dtype.to_message(1, [True, b"abc"])
    with pytest.raises(TypeError):
        dtype.to_message(1, [b"abc"], "abc")


def test_structure_in_property():
    f = Feature(get_feature_definition_str("StructInProperty"))
    prop = f._unobservable_properties["TestProperty"]
    assert isinstance(prop, UnobservableProperty)

    assert isinstance(prop.data_type, Structure)
    assert prop.to_native_type(prop.to_message(["abc", 1])) == ("abc", 1)


def test_structure_in_command():
    f = Feature(get_feature_definition_str("StructInCommand"))
    cmd = f._observable_commands["TestCommand"]
    assert isinstance(cmd, ObservableCommand)

    par = cmd.parameters
    assert len(par) == 1
    assert isinstance(par.fields[0].data_type, Structure)
    assert par.to_native_type(par.to_message(["abc", 1]))[0] == ("abc", 1)

    res = cmd.responses
    assert len(res) == 1
    assert isinstance(res.fields[0].data_type, Structure)
    assert res.to_native_type(res.to_message(["abc", 1]))[0] == ("abc", 1)

    ires = cmd.intermediate_responses
    assert len(ires) == 1
    assert isinstance(ires.fields[0].data_type, Structure)
    assert ires.to_native_type(ires.to_message(["abc", 1]))[0] == ("abc", 1)
