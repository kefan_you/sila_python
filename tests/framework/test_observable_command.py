import uuid
from typing import Dict
from uuid import UUID

import pytest
from google.protobuf.message import Message

from sila2.framework import ValidationError
from sila2.framework.command.command_confirmation import CommandConfirmation
from sila2.framework.command.command_execution_uuid import CommandExecutionUUID
from sila2.framework.command.execution_info import CommandExecutionInfo, CommandExecutionStatus, ExecutionInfo
from sila2.framework.command.observable_command import ObservableCommand
from sila2.framework.feature import Feature
from tests.utils import create_server_stub, get_feature_definition_str


@pytest.fixture()
def observable_command_feature():
    return Feature(get_feature_definition_str("ObservableCommand"))


def test(observable_command_feature):
    cmd = observable_command_feature._observable_commands["SendCharacters"]

    assert isinstance(cmd, ObservableCommand)
    assert cmd._identifier == "SendCharacters"
    assert cmd._display_name == "Send characters"
    assert cmd._description == "Takes a string and returns a stream of its characters"
    assert cmd.fully_qualified_identifier == "de.unigoettingen/tests/ObservableCommand/v1/Command/SendCharacters"

    with pytest.raises(TypeError):
        cmd.intermediate_responses.to_message("a", 2)

    with pytest.raises(ValidationError):
        cmd.intermediate_responses.to_message("ab")


def test_empty_intermediate_responses(observable_command_feature):
    empty_cmd = observable_command_feature._observable_commands["NoIntermediateResponses"]
    assert isinstance(empty_cmd, ObservableCommand)
    assert len(empty_cmd.responses) == 0
    assert len(empty_cmd.parameters) == 0
    assert empty_cmd.intermediate_responses is None


def test_nested_messages(observable_command_feature):
    cmd = observable_command_feature._observable_commands["NestedTypes"]

    assert isinstance(cmd, ObservableCommand)

    for message_map in (cmd.parameters, cmd.intermediate_responses, cmd.responses):
        msg = message_map.to_message(1, [("abc", True), ("def", False)])
        assert isinstance(msg, Message)
        a, (b, c) = message_map.to_native_type(msg)
        assert a == 1
        assert b == ("abc", True)
        assert c == ("def", False)


def test_grpc(observable_command_feature, silaframework_pb2_module):
    cmd = observable_command_feature._observable_commands["SendCharacters"]
    assert isinstance(cmd, ObservableCommand)

    # NOTE: This is not a correct implementation! It is just here for checking if grpc interaction is working properly.
    class ObservableCommandImpl(observable_command_feature._servicer_cls):
        def __init__(self):
            self.send_character_instances: Dict[UUID, str] = {}
            self.send_character_progress: Dict[UUID, CommandExecutionInfo] = {}

        def SendCharacters(self, request, context):
            (string,) = cmd.parameters.to_native_type(request)
            exec_id = uuid.uuid4()
            self.send_character_instances[exec_id] = string
            self.send_character_progress[exec_id] = CommandExecutionInfo(CommandExecutionStatus.running, 0, None, None)
            return CommandConfirmation(silaframework_pb2_module).to_message(exec_id)

        def SendCharacters_Intermediate(self, request, context):
            exec_id = CommandExecutionUUID(silaframework_pb2_module).to_native_type(request)
            string = self.send_character_instances[exec_id]
            for i, char in enumerate(string):
                yield cmd.intermediate_responses.to_message(char)
                self.send_character_progress[exec_id] = CommandExecutionInfo(
                    CommandExecutionStatus.running, i / len(string), None, None
                )
            self.send_character_progress[exec_id] = CommandExecutionInfo(
                CommandExecutionStatus.finishedSuccessfully, 1, None, None
            )

        def SendCharacters_Info(self, request, context):
            exec_id = CommandExecutionUUID(silaframework_pb2_module).to_native_type(request)
            while self.send_character_progress[exec_id].status == CommandExecutionStatus.running:
                yield ExecutionInfo(silaframework_pb2_module).to_message(self.send_character_progress[exec_id])
            yield ExecutionInfo(silaframework_pb2_module).to_message(self.send_character_progress[exec_id])

        def SendCharacters_Result(self, request, context):
            return cmd.responses.to_message()

    server, stub = create_server_stub(observable_command_feature._grpc_module, ObservableCommandImpl())
    exec_id, _ = CommandConfirmation(silaframework_pb2_module).to_native_type(
        stub.SendCharacters(cmd.parameters.to_message("abcdef"))
    )
    assert isinstance(exec_id, UUID)
    info_stream = stub.SendCharacters_Info(CommandExecutionUUID(silaframework_pb2_module).to_message(exec_id))
    assert [
        cmd.intermediate_responses.to_native_type(res).Character
        for res in stub.SendCharacters_Intermediate(CommandExecutionUUID(silaframework_pb2_module).to_message(exec_id))
    ] == list("abcdef")
    assert (
        len(
            cmd.responses.to_native_type(
                stub.SendCharacters_Result(CommandExecutionUUID(silaframework_pb2_module).to_message(exec_id))
            )
        )
        == 0
    )

    infos = [ExecutionInfo(silaframework_pb2_module).to_native_type(info) for info in info_stream]
    assert infos[-1].status == CommandExecutionStatus.finishedSuccessfully
