from typing import Dict

from sila2.framework.fully_qualified_identifier import FullyQualifiedIdentifier


def test():
    raw_identifier = "de.unigoettingen/tests/ObservableProperty/v1"
    identifier = FullyQualifiedIdentifier(raw_identifier)

    assert raw_identifier == identifier
    assert identifier == raw_identifier
    assert FullyQualifiedIdentifier(raw_identifier.lower()) == raw_identifier.upper()
    assert raw_identifier.upper() == FullyQualifiedIdentifier(raw_identifier.lower())

    assert identifier != "abc"
    assert identifier != 123

    assert repr(identifier) == f"FullyQualifiedIdentifier('{raw_identifier}')"

    d: Dict[FullyQualifiedIdentifier, int] = {}
    d[FullyQualifiedIdentifier("abc")] = 1
    d[FullyQualifiedIdentifier("Abc")] = 2
    assert len(d) == 1
    assert d[FullyQualifiedIdentifier("ABC")] == 2
