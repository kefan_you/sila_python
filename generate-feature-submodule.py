#!/usr/bin/env python
from os.path import abspath, dirname, join

import sila2
from sila2.code_generator.__main__ import generate_feature_files

base_dir = dirname(abspath(__file__))
fdl_files = [
    f"{base_dir}/sila_base/feature_definitions/org/silastandard/core/AuthenticationService-v1_0.sila.xml",
    f"{base_dir}/sila_base/feature_definitions/org/silastandard/core/AuthorizationProviderService-v1_0.sila.xml",
    f"{base_dir}/sila_base/feature_definitions/org/silastandard/core/AuthorizationService-v1_0.sila.xml",
    f"{base_dir}/sila_base/feature_definitions/org/silastandard/core/LockController-v1_0.sila.xml",
    f"{base_dir}/sila_base/feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml",
]

generate_feature_files(
    output_directory=join(dirname(sila2.__file__), "features"),
    overwrite=True,
    feature_definitions=[abspath(f) for f in fdl_files],
)
