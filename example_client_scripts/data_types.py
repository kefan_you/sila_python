from pathlib import Path

from sila2_example_server import Client

from sila2.framework.data_types.date import SilaDateType


def main():
    certificate_authority = Path("ca.pem").read_bytes()
    client = Client("127.0.0.1", 50052, root_certs=certificate_authority)

    # structure with two elements: a Binary and a List of Dates
    response = client.DataTypeProvider.StructureProperty.get()
    print(response)

    binary, dates = response
    assert binary == b"abc"
    assert isinstance(dates, list)
    assert all(isinstance(date, SilaDateType) for date in dates)

    # command with two parameters:
    # - Number: a Custom Data Type 'IntegerAlias' as alias for a basic Integer
    # - Structure: a Structure with two fields:
    #   - ListOfStrings: a List of Strings
    #   - Boolean: a Boolean
    num = 123
    strings = ["abc", "def", "ghi"]
    response = client.DataTypeProvider.ComplexCommand(num, (strings, True))
    print(response)

    # response has two fields:
    # - Structure of List of Strings and Boolean (same custom type as Structure parameter)
    # - Structure of String and Real
    (strings, boolean), (string, real) = response
    assert isinstance(strings, list)
    assert all(isinstance(item, str) for item in strings)
    assert isinstance(boolean, bool)
    assert isinstance(string, str)
    assert isinstance(real, float)


if __name__ == "__main__":
    main()
