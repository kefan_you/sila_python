from pathlib import Path

from sila2_example_server import Client

from sila2.framework import InvalidMetadata


def main():
    certificate_authority = Path("ca.pem").read_bytes()
    client = Client("127.0.0.1", 50052, root_certs=certificate_authority)

    # SiLA Client Metadata can be sent to SiLA Servers attached to command invocation and property access/subscription.
    # Metadata can be specified as keyword-only argument and, if given, must be a dictionary.
    # The dictionary keys must be the metadata objects, or their fully qualified identifiers.

    # In this example, the Delay metadata of the DelayProvider feature is used to delay acces to the property
    # `RandomNumber` by N milliseconds

    print("Affected calls:", client.DelayProvider.Delay.get_affected_calls())

    # Metadata is sent as a dictionary
    print("Calling with delay:")
    print(
        "RandomNumber with delay:", client.DelayProvider.RandomNumber.get(metadata=[client.DelayProvider.Delay(1000)])
    )

    # When required metadata is not sent, a InvalidMetadata error is raised by the server
    try:
        client.DelayProvider.RandomNumber.get()
    except InvalidMetadata as ex:
        print("Caught InvalidMetadata error:", ex)


if __name__ == "__main__":
    main()
