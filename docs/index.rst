Python implementation of the SiLA 2 standard
============================================

SiLA 2
------

`SiLA 2 <https://sila-standard.com>`_ is an open communication standard for laboratory automation.

Find the SiLA 2 specification here:

- `Part A <https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM>`_ - Overview, Concepts and Core Specification
- `Part B <https://docs.google.com/document/d/1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA>`_ - Mapping Specification

Join the SiLA 2 Slack Community `here <https://sila-standard.slack.com/join/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA>`_.

Installation
------------

Run ``pip install sila2`` to install the latest release from `PyPI <https://pypi.org/project/sila2>`_.

Documentation
-------------

This section explains how to connect to SiLA Servers and interact with them via SiLA Clients.

.. toctree::
    :maxdepth: 2

    content/client/index
    content/server/index
    content/data_types/index
    content/api/index
    content/code_generator/index
