Framework API
-------------

.. toctree::
    :maxdepth: 3

    fully_qualified_identifier
    command_execution_status
    feature/index
    errors/index
    data_types
