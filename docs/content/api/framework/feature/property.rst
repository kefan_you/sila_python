Class Property
==============

.. autoclass:: sila2.framework.Property

Class UnobservableProperty
==========================

.. autoclass:: sila2.framework.UnobservableProperty
    :show-inheritance:

Class ObservableProperty
========================

.. autoclass:: sila2.framework.ObservableProperty
    :show-inheritance:
