Class Command
=============

.. autoclass:: sila2.framework.Command

.. autoclass:: sila2.framework.UnobservableCommand
    :show-inheritance:

.. autoclass:: sila2.framework.ObservableCommand
    :show-inheritance:

.. autoclass:: sila2.framework.Parameter

.. autoclass:: sila2.framework.IntermediateResponse

.. autoclass:: sila2.framework.Response
