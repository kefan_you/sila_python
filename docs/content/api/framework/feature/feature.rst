Class Feature
=============

.. autoclass:: sila2.framework.Feature
    :special-members: __getitem__
