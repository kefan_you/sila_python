Class CommandExecutionStatus
============================

.. autoclass:: sila2.framework.CommandExecutionStatus
    :undoc-members:
    :show-inheritance:
