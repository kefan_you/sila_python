Data Type API
=============

.. autoclass:: sila2.framework.SilaDateType
    :show-inheritance:

.. autoclass:: sila2.framework.SilaAnyType
    :show-inheritance:
