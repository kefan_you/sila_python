Class ClientUnobservableCommand
===============================

.. autoclass:: sila2.client.ClientUnobservableCommand
    :special-members: __call__
