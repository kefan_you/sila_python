#!/usr/bin/env python
# ruff: noqa: T201,S605 - print, possible shell injection
import os
import sys

for command in [
    f"{sys.executable} -m ruff format --check .",
    f"{sys.executable} -m ruff check .",
    f"{sys.executable} -m pytest --cov=src --cov-config=pyproject.toml tests/",
]:
    print(f"Running {command!r}")
    exit_code = os.system(command)
    if exit_code != 0:
        sys.exit(exit_code)
