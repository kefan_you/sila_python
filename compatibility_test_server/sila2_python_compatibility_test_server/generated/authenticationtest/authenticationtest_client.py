# Generated by sila2.code_generator; sila2.__version__: 0.10.2
# -----
# This class does not do anything useful at runtime. Its only purpose is to provide type annotations.
# Since sphinx does not support .pyi files (yet?), so this is a .py file.
# -----

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Iterable, Optional

    from authenticationtest_types import RequiresToken_Responses, RequiresTokenForBinaryUpload_Responses

    from sila2.client import ClientMetadataInstance


class AuthenticationTestClient:
    """

    Contains commands that require authentication. A client should be able to obtain an Authorization Token through the Login command of the Authentication Service feature
    using the following credentials: username: 'test', password: 'test'

    """

    def RequiresToken(self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> RequiresToken_Responses:
        """
        Requires an authorization token in order to be executed
        """
        ...

    def RequiresTokenForBinaryUpload(
        self, BinaryToUpload: bytes, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> RequiresTokenForBinaryUpload_Responses:
        """
        Requires an authorization token in order to be executed and to upload a binary parameter
        """
        ...
