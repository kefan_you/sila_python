from pathlib import Path

from grpc_tools import protoc

root_dir = Path(__file__).parent
sila_base_dir = root_dir / "sila_base"
out_dir = root_dir / "src" / "sila2" / "framework" / "pb2"


if __name__ == "__main__":
    protoc_args = [
        "protoc",  # protoc expects args[0] to be the program name, which is irrelevant here and will be ignored
        f"--pyi_out={out_dir}",
        f"--python_out={out_dir}",
        f"--grpc_python_out={out_dir}",
        f"--proto_path={sila_base_dir / 'protobuf'}",
        str(sila_base_dir / "protobuf" / "SiLAFramework.proto"),
        str(sila_base_dir / "protobuf" / "SiLABinaryTransfer.proto"),
    ]
    if protoc.main(protoc_args) != 0:
        raise RuntimeError(f"Failed to run {protoc_args}")
